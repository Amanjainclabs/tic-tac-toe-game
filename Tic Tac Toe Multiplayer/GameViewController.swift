import UIKit
var countDraws = 0
var countWinsPlayerOne = 0
var countWinsPlayerTwo = 0
class GameViewController: UIViewController {
  
  var scoreArray = [String]()
  var getScore = String()
  // 0 - Empty, 1 - Nought, 2 - Cross
  var gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
  var nextNumber = 0
  // Possible combinations
  let winningCombinations = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
  // check whether someone has one or not
  var winner = 0
  // show a strikethrough line on winning combination
  @IBOutlet weak var winningBoardImage: UIImageView!
  
  @IBOutlet weak var playAgain: UIButton!
  @IBOutlet weak var resultLabel: UILabel!
  
  // when any position in the grid is pressed
  @IBAction func buttonPressed(sender: AnyObject) {
    /*  The buttons in the grid have been tagged as follows:
    1|2|3
    4|5|6
    7|8|9
    When a particular button is pressed it's state changes
    */
    if gameState[sender.tag - 1] == 0 && winner == 0 {
      var image = UIImage(named: "")
      
      // nextNumber is incremented by 1 everytime a button is pressed so that alternate 0 and X can appear
      if nextNumber%2 == 0 {
        image = NoughtImage
        gameState[sender.tag - 1] = 1
      } else {
        image = CrossImage
        gameState[sender.tag-1] = 2
      }
      
      // check for match of any possible pattern
      for combination in winningCombinations {
        if (gameState[combination[0]] == gameState[combination[1]] && gameState[combination[1]] == gameState[combination[2]] && gameState[combination[0]] != 0) {
          winner = gameState[combination[0]]
          // if combination matches any winning combination change image of board matching it
          for winCombination in 0...7 {
            if (winningCombinations[winCombination] == combination) {
              winningBoardImage.image = UIImage(named: "combination\(winCombination).png")
            }
          }
        }
      }
      
      // Check whether tie has occured or not
      var isFull = 0
      for status in 0...8 {
        if gameState[status] != 0 {
          isFull++
        }
      }
      
      if isFull == 9 && winner == 0 {
        resultLabel.text = "Game is a Tie!"
        countDraws++
        countTotalGames++
        UIView.animateWithDuration(1, animations: {
          self.resultLabel.center = CGPointMake(self.resultLabel.center.x + 400, self.resultLabel.center.y)
          self.resultLabel.hidden = false
          self.playAgain.hidden = false
        })
      }
      
      // If game is not tie and there ia winner
      if winner != 0 {
        // If winner has the same state as of the player 1
        if winner == PlayerState {
          resultLabel.text = "\(PlayerOneName) is the winner!"
          countWinsPlayerOne++
          countTotalGames++
        } else {
          resultLabel.text = "\(PlayerTwoName) is the winner!"
          countWinsPlayerTwo++
          countTotalGames++
        }
        
        UIView.animateWithDuration(1, animations: {
          self.resultLabel.center = CGPointMake(self.resultLabel.center.x + 400, self.resultLabel.center.y)
          self.resultLabel.hidden = false
          self.playAgain.hidden = false
        })        
        
      }
      
      // Change the image of the button according to the turn
      sender.setImage(image, forState: .Normal)
      nextNumber++
    }
  }
  
  // Reset all variables when play again button is pressed
  @IBAction func playAgainPressed(sender: AnyObject) {
    winningBoardImage.image = UIImage(named: "tictactoe.png")
    gameState = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    winner = 0
    nextNumber = GoNumber
    UIView.animateWithDuration(1, animations: {
      self.resultLabel.center = CGPointMake(self.resultLabel.center.x - 400, self.resultLabel.center.y)
      self.resultLabel.hidden = true
      self.playAgain.hidden = false
    })
    
    playAgain.hidden = true
    // Remove all the images from the buttons
    var button = UIButton()
    for tag in 1...10 {
      button = view.viewWithTag(tag) as UIButton
      button.setImage(nil, forState: .Normal)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    winningBoardImage.image = UIImage(named: "tictactoe.png")
    // If no names are chosen by the players then provide them default names
    if PlayerOneName == "" {
      PlayerOneName = "Player 1"
    }
    
    if PlayerTwoName == "" {
      PlayerTwoName = "Player 2"
    }
    nextNumber = GoNumber
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewDidAppear(animated: Bool) {
    resultLabel.center = CGPointMake(resultLabel.center.x - 400, resultLabel.center.y)
  }
  
  @IBAction func compileScoreList(sender: AnyObject) {
    
    // if memory is non empty
    if var storedScoreList : AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("ScoresList") {
      // store the memory contents in the string array
      if (storedScoreList.count) != nil {
        for items in 0...storedScoreList.count - 1 {
          scoreArray.append(storedScoreList[items] as String)
        }
      } else {
        // if memory is empty then make the first element of array nil
        scoreArray.append("")
      }
    }
    
    // if the number of games played is not zero
    if countTotalGames != 0 {
      // creating a format in strings to display data in scoreboard
      var scoreStringPlayer1 = "\(countWinsPlayerOne)\t\t" + "\(countDraws)\t\t" + "\(countTotalGames)"
      var scoreStringPlayer2 = "\(countWinsPlayerTwo)\t\t" + "\(countDraws)\t\t" + "\(countTotalGames)"
      var competitorsNameString = "\(PlayerOneName)\tvs\t\(PlayerTwoName)\n\n"
      var scoreCellPlayer1 = "Player 1:\t " + scoreStringPlayer1 + "\n\n"
      var scoreCellPlayer2 = "Player 2:\t " + scoreStringPlayer2 + "\n\n"
      // concatinate the data and append to the array
      getScore = competitorsNameString + scoreCellPlayer1 + scoreCellPlayer2
      scoreArray.append(getScore)
      // immutable array so that data can be stored in memory
      let storeScoreLog = scoreArray
      NSUserDefaults.standardUserDefaults().setObject(storeScoreLog, forKey: "ScoresList")
      NSUserDefaults.standardUserDefaults().synchronize()
      // reset all scores after storing in memory
      countDraws = 0
      countWinsPlayerOne = 0
      countWinsPlayerTwo = 0
      countTotalGames = 0
    }
  }
}