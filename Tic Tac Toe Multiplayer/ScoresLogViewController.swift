//
//  ScoresLogViewController.swift
//  Tic Tac Toe Multiplayer
//
//  Created by Click Labs on 2/9/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

import UIKit
class ScoresLogViewController: UIViewController {
  
  @IBOutlet weak var scoresLogTable: UITextView!
  override func viewDidLoad() {
    super.viewDidLoad()
    // store the data from persistence storage memory into a string array
    if var storedScoreList : AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("ScoresList") {
      var scoreArray = [String]()
      // if memory is non empty then append memory into array
      if (storedScoreList.count) != nil {
        for items in 0...storedScoreList.count - 1 {
          scoreArray.append(storedScoreList[items] as String)
        }
      
        var createList = "\t\t" + "WIN\t" + "\tDRAW\t" + "TOTAL" + "\n\n"
        // creating a format of string to be displyed on the text view
        for items in stride(from: scoreArray.count - 1, to: 0, by: -1) {
          createList = createList + "\(scoreArray.count - items).\t" + scoreArray[items] + "\n"
        }
      
        // passing the created string to the textview outlet
        scoresLogTable.text = createList
      }
    }
  }
  
  // function called when reset log button is pressed
  @IBAction func resetScoreLog(sender: AnyObject) {
    
    // clearing persistence storage memory
    let storeScoreLog = [""]
    NSUserDefaults.standardUserDefaults().setObject(storeScoreLog, forKey: "ScoresList")
    NSUserDefaults.standardUserDefaults().synchronize()
    // clearing the textview
    scoresLogTable.text = ""
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}