import UIKit

// GoNumber tells which player is moving first and whose turn is next
var GoNumber = 1
// PlayerState tells what piece choice player 1 made
var PlayerState = 2
// Names of players
var PlayerOneName = String()
var PlayerTwoName = String()
// change t he image in the playing area
var CrossImage = UIImage()
var NoughtImage = UIImage()

class PlayersDetailsViewController: UIViewController, UITextFieldDelegate {
  
  // store various styles of X and 0 in form of a view
  @IBOutlet weak var crossStyleView: UIView!
  @IBOutlet weak var noughtStyleView: UIView!
  
  @IBOutlet weak var playerOneName: UITextField!
  @IBOutlet weak var playerTwoName: UITextField!
  
  // represents the choice made by player 1
  @IBOutlet weak var crossButton: UIButton!
  @IBOutlet weak var noughtButton: UIButton!
  // button used to choose different styles of buttons
  @IBOutlet weak var crossStyleButton: UIButton!
  @IBOutlet weak var noughtStyleButton: UIButton!
  
  // opens menu to select styles of crosses
  @IBAction func crossStylePressed(sender: AnyObject) {
    UIView.animateWithDuration(0.5, animations: {
      self.crossStyleView.alpha = 1
      self.noughtStyleView.alpha = 0
    })
  }
  
  // opens menu to select styles of noughts
  @IBAction func noughtStylePressed(sender: AnyObject) {
    UIView.animateWithDuration(0.5, animations: {
      self.noughtStyleView.alpha = 1
      self.crossStyleView.alpha = 0
    })
  }
  
  // choose any one of the styles of cross
  @IBAction func crossChoices(sender: AnyObject) {
    // capture the tag of the button and change image of all visible crosses
    var buttonTag = sender.tag % 10
    CrossImage = UIImage(named: "cross\(buttonTag)")!
    crossStyleView.alpha = 0
    crossButton.setImage(CrossImage, forState: .Normal)
    crossStyleButton.setImage(CrossImage, forState: .Normal)
  }
  
  // choose any one of the styles of nought
  @IBAction func noughtChoices(sender: AnyObject) {
    // capture the tag of the button and change image of all visible noughts
    var buttonTag = sender.tag % 10
    NoughtImage = UIImage(named: "nought\(buttonTag)")!
    noughtStyleView.alpha = 0
    noughtButton.setImage(NoughtImage, forState: .Normal)
    noughtStyleButton.setImage(NoughtImage, forState: .Normal)
  }
  
  // Choice made by player 1 and highlight the choice
  @IBAction func firstMoveCross(sender: AnyObject) {
    GoNumber = 1
    crossButton.backgroundColor = UIColor.yellowColor()
    noughtButton.backgroundColor = UIColor.clearColor()
    PlayerState = 2
    crossStyleView.alpha = 0
    noughtStyleView.alpha = 0
  }
  
  @IBAction func firstMoveNought(sender: AnyObject) {
    GoNumber = 2
    noughtButton.backgroundColor = UIColor.yellowColor()
    crossButton.backgroundColor = UIColor.clearColor()
    PlayerState = 1
    crossStyleView.alpha = 0
    noughtStyleView.alpha = 0
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // provide default vales
    CrossImage = UIImage(named: "cross0.png")!
    NoughtImage = UIImage(named: "nought0")!
    crossButton.setImage(CrossImage, forState: .Normal)
    crossStyleButton.setImage(CrossImage, forState: .Normal)
    noughtButton.setImage(NoughtImage, forState: .Normal)
    noughtStyleButton.setImage(NoughtImage, forState: .Normal)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func textFieldShouldReturn(textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
    self.view.endEditing(true)
    crossStyleView.alpha = 0
    noughtStyleView.alpha = 0
  }
  
  // Transfer the details to second view controller and begin the game
  @IBAction func beginGame(sender: AnyObject) {
    PlayerOneName = playerOneName.text
    PlayerTwoName = playerTwoName.text
    countDraws = 0
    countWinsPlayerOne = 0
    countWinsPlayerTwo = 0
    countTotalGames = 0
  }
  
  override func viewDidAppear(animated: Bool) {
    crossStyleView.alpha = 0
    noughtStyleView.alpha = 0
  } 
}