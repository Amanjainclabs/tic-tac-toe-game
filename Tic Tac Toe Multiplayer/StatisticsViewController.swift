import UIKit
var countTotalGames = 0
class StatisticsViewController: UIViewController {
  
  @IBOutlet weak var playerOneWinCount: UILabel!
  @IBOutlet weak var playerTwoWinCount: UILabel!
  @IBOutlet weak var drawsCount: UILabel!
  @IBOutlet weak var totalGamesCount: UILabel!
  @IBOutlet weak var playerOneName: UILabel!
  @IBOutlet weak var playerTwoName: UILabel!
  @IBOutlet weak var playerOneLossCount: UILabel!
  @IBOutlet weak var playerTwoLossCount: UILabel!
  
  // function to display contents in the view controller
  func displayScores() {
    totalGamesCount.text = String(countTotalGames)
    playerOneWinCount.text = String(countWinsPlayerOne)
    playerTwoWinCount.text = String(countWinsPlayerTwo)
    drawsCount.text = String(countDraws)
    playerOneLossCount.text = playerTwoWinCount.text
    playerTwoLossCount.text = playerOneWinCount.text
    
    // display name only when something is entered in textfields
    if PlayerOneName == "Player 1" {
      playerOneName.text = ""
    } else {
      playerOneName.text = PlayerOneName
    }
    
    if PlayerTwoName == "Player 2" {
      playerTwoName.text = ""
    } else {
      playerTwoName.text = PlayerTwoName
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    displayScores()
  }
  
  // reset the scoreboard
  @IBAction func resetScores(sender: AnyObject) {
    countDraws = 0
    countWinsPlayerOne = 0
    countWinsPlayerTwo = 0
    displayScores()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
  }
}
